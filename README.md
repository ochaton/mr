# MapReduce for vshard

This library is a prototype.

`mr` allows to code map-reduce tasks to perform heavy calculations on cluster

For now `mr` supports only 2 phases: Map-phase and Reduce-phase in Hadoop meaning.

## Example

```lua

local job = require 'mr' {
    -- You better specify name of your job
    job_name = 'count_users_in_groups',
    -- You must specify single tarantool space to look through on
    space = 'users',

    -- User defined context:
    users_in_group = {}, -- (assume that we do not have a lot of groups.)
}
:map(function(self, user) -- (user) => <group_name>, <count>
    -- Map callback is called on Storage.
    -- This callback is heaviest part of all job. Write it perfomant.
    local group = user.group
    if group == nil then
        group = 'no_group'
    end
    if not users_in_group[group] then
        users_in_group[group] = 0
    end
    users_in_group[group] = users_in_group[group] + 1

    -- Actually we do map+combine. And you also should do this.
    -- Sending large data from map to reduce phase is not supported now.
    return group, users_in_group[group]
end)
:reduce(function(self, group_name, user_counts) -- {group_name, user_counts} => {group_name, total_user_counts}
    -- reduce phase is run on executor (the node you run this job)
    -- we may reuse user defined variables. They would be empty, because each node has own set of variables
    if not users_in_group[group_name] then
        users_in_group[group_name] = 0
    end

    -- now we add up user_counts from each mapper
    users_in_group[group_name] = users_in_group[group_name] + user_counts

    -- values with group_name can be duplicated. They are not grouped by key to speed up perfomance

    -- and return result
    return group_name, user_in_group[group_name]
end)

-- Now we can run our job:
job:run()

-- You can get status of job:
job:get_status()

-- When job will be finished you may get result:
job:get_result()

-- If wait for job is too long, you may cancel it:
job:cancel()

-- If any error happens (on Mapper or on Reducer) you may get it
job:get_error()

-- You may wait for result for some time
job:wait_result_or_error(60) -- and setup timeout in seconds. Function will return :get_status()

```

## Job configuration

```lua
require 'mr' {
    job_name = 'map_reduce_job',
    space = 'transactions',

    sleep_every = 700, -- you may adjust yields on Mapper. (max: 700)
    sleep_seconds = 0.001, -- you may adjust sleep timeout in seconds between yield on Mapper (min: 0.001)
    deadline = os.time()+600, -- you may setup deadline for Mapper. (default: 3600 seconds)
}
```
