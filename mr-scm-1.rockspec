package = "mr"
version = "scm-1"
source = {
   url = "https://gitlab.com/ochaton/mr.git",
}
description = {
   homepage = "https://gitlab.com/ochaton/mr",
   summary = "Map-Reduce for Tarantool vshard",
   license = "MIT"
}
dependencies = {
   "lua ~> 5.1"
}
build = {
   type = "builtin",
   modules = {
      mr = "mr.lua",
      ["mr.executor"] = "mr/executor.lua",
   }
}
