local M = {}
local methods

local ffi = require 'ffi'
local log = require 'log'
local clock = require 'clock'
local vshard = require 'vshard'
local fiber = require 'fiber'

local decimal do
	local ok, v = pcall(require, 'decimal')
	if ok then decimal = v end
end

local function list2kv(list)
	local r = {}
	for _, v in pairs(list) do
		r[v] = true
	end
	return r
end

local basic_types = list2kv{'number','string','boolean','nil'}
local unsupported_types = list2kv{'userdata','thread'}

local function is_decimal(v)
	return type(v) == 'cdata' and pcall(decimal.new, v)
end

local function compile_function(name, what)
	assert(type(what) == 'function')

	local up_name, _ = debug.getupvalue(what, 1)
	if up_name then
		error("Cant compile function "..name.." with upvalue: "..up_name)
	end

	return { t = 'function', v = string.dump(what) }
end

local function compile_cdata(name, what)
	assert(type(what) == 'cdata')

	local ffi_type = tostring(ffi.typeof(what))
	if ffi_type == 'ctype<int64_t>' then
		return what
	elseif ffi_type == 'ctype<uint64_t>' then
		return what
	elseif ffi_type == 'ctype<struct tt_uuid>' then
		return what
	elseif is_decimal(what) then
		return what
	else
		error("Unsupported cdata: "..name.." type: "..ffi_type)
	end
end

local function compile(name, what)
	local lua_type = type(what)
	if unsupported_types[lua_type] then
		error("Cant compile "..name.." type: "..lua_type)
	end
	if basic_types[lua_type] then
		return { v = what, t = lua_type }
	end

	if lua_type == 'function' then
		return compile_function(name, what)
	elseif lua_type == 'table' then
		local r = {}
		for key, value in pairs(what) do
			r[key] = compile(name..'.'..tostring(key), value)
		end
		return { v = r, t = 'table' }
	elseif lua_type == 'cdata' then
		return { v = compile_cdata(name, what), t = 'cdata' }
	else
		error("Cant compile "..name.. " unsupported lua type: "..lua_type)
	end
end


function M.new(self, args)
	if self ~= M then
		args = self
	end
	args = args or {}

	if not args.space then
		error("mr: space is required")
	end

	local mr = setmetatable(args, { __index = methods })

	local compiled = {}

	math.randomseed(tonumber(clock.time()))
	mr.job_id = 'mr_'..math.random(1, 1e6)
	mr.job_name = mr.job_name or 'mr_job'

	for key, value in pairs(mr) do
		compiled[key] = compile('mr.'..key, value)
	end

	mr.__obj = compiled

	return mr
end

setmetatable(M, { __call = M.new })

methods = {}

---Mapper runs on Storage
---<k1, v1> => list(<k2, v2>)
function methods:map(func)
	if not debug.getlocal(func, 2) then
		error("mr:map function must contain at least 2 arguments")
	end

	assert(not self.__obj.mapper, "mapper is already set")

	self.__obj.mapper = compile_function('mr.map', func)
	return self
end

---Final stage
---Runs on executor
---<k2, list(v2)> => list(<k3, v3>)
function methods:reduce(func)
	if not debug.getlocal(func, 3) then
		error("mr:reduce function must contain at least 3 arguments")
	end

	assert(not self.reducer, "reducer is already set")

	self.reducer = table.deepcopy(self)
	self.reducer.reduce = func

	return self
end

function methods:run()
	-- Execute of map/reduce

	---@diagnostic disable-next-line: undefined-field
	if not next(vshard.router.internal.routers) then
		error("You are not on vshard router")
	end

	local storages = vshard.router.routeall()
	local replicas = {}

	for ruuid, rs in pairs(storages) do
		for iuuid, r in pairs(rs.replicas) do
			if iuuid ~= rs.master.uuid and r:is_connected() then
				replicas[ruuid] = r
				log.info("Choosing replica %s for rs", iuuid, ruuid)
				break
			end
		end
		if not replicas[ruuid] then
			error("Replica for rs "..ruuid.." is not discovered")
		end
	end

	self.replicas = replicas
	self.stage = 'initial'
	self.error = {}

	self.controller_f = require 'background' {
		name = 'mr/'..self.job_name,
		restart = false,
		run_interval = 1,
		wait = false,
		args = { self },
		teardown = function()
			self:teardown()
		end,
		on_fail = function(err)
			if self.stage == 'reduce' then
				self.reduce_status = self.reduce_status or {}
				self.reduce_status.ok = false
				self.reduce_status.error = err
			end
		end,
		run_while = function() return not self.stopped end,
		func = function(_, mr)
			mr:controller_loop()
		end,
	}
end

function methods:teardown()
	if self.stage == 'stopped' then
		log.warn("already stopped")
	end
	for ruuid, r in pairs(self.replicas) do
		local job_func = 'box.session.storage.'..self.job_id..'.teardown'
		local ok, res = pcall(r.conn.call, r.conn, job_func)
		if not ok then
			log.warn("teardown on replicaset %s failed: %s", ruuid, res)
		else
			log.info("teardown on %s - ok", ruuid)
		end
	end

	self.stage = 'stopped'
end

function methods:run_map()
	local obj = self.__obj

	local setup_func = require 'mr.executor'.setup
	local func = compile_function('mr.setup', setup_func).v

	self.map_status = {}

	for ruuid, r in pairs(self.replicas) do
		local show_status = r.conn:eval([[
			local func, arg = ...
			local executor = loadstring(func)
			return executor(arg)
		]], { func, obj }, { timeout = 10 })

		self.map_status[ruuid] = show_status
	end
end

function methods:get_map_status()
	for ruuid, r in pairs(self.replicas) do
		local job_func = 'box.session.storage.'..self.job_id..'.get_status'
		self.map_status[ruuid] = r.conn:call(job_func)
	end
	return self.map_status
end

function methods:get_status()
	return {
		ok = self.ok,
		stage = self.stage,
		map = self.map_status,
		reduce = self.reduce_status,
		error = self.error,
	}
end

function methods:get_error()
	return next(self.error) and self.error
end

function methods:run_reduce()
	-- Stops background
	self.stopped = true
	local result = {}
	self.reduce_status = {
		run_at = clock.time(),
		fiber_id = fiber.id(),
		job_id = self.job_id,
	}

	for ruuid, res in pairs(self.map_result) do
		self.reduce_status.processing = ruuid
		if self.cancelled then
			self.reduce_status.ok = false
			self.reduce_status.error = 'job was cancelled'
			self.reduce_status.result = result
			self.error["reduce"] = "job was cancelled"
			error("job was cancelled")
		end
		for key, value in pairs(res) do
			local rk, rv = self.reducer:reduce(key, value)
			result[rk] = rv
		end
		fiber.sleep(self.sleep_seconds or 0.001)
	end

	self.reduce_status.ok = true
	self.reduce_status.result = result
	self.result = result
	return result
end

function methods:get_result()
	return self.result
end

function methods:wait_result_or_error(to)
	local deadline = fiber.time()+(to or 3600)
	while not self.error and not self.result do
		if deadline < fiber.time() then break end
		fiber.sleep(1)
	end
	return self:get_status()
end

function methods:cancel()
	self.cancelled = true
	if self.stage == 'wait_map_status' then
		for ruuid, r in pairs(self.replicas) do
			log.info("Cancelling job on %s/%s", ruuid, r.uuid)
			local job_func = 'box.session.storage.'..self.job_id..'.cancel'
			r.conn:call(job_func)
		end
		self.stage = 'stopped'
		return self:get_map_status()
	end
end

function methods:controller_loop()
	if self.stage == 'initial' then
		self:run_map()
		self.stage = 'wait_map_status'
	end

	if self.stage == 'wait_map_status' then
		local status = self:get_map_status()
		local oks, noks, in_progress = 0, 0, 0
		local errors = {}
		local result = {}
		for ruuid, s in pairs(status) do
			if s.ok then
				oks = oks + 1
				result[ruuid] = s.result
			elseif s.ok == false then
				noks = noks + 1
				errors[ruuid] = s.error
			else
				in_progress = in_progress + 1
			end
		end

		if in_progress > 0 then
			-- someone is still in progress
			return
		end

		if noks > 0 then
			for k, v in pairs(errors) do
				self.error[k] = v
			end
			log.error("Recevied %d errors. Terminating job on mappers", noks)
			self:cancel()
		end

		if in_progress == 0 and noks == 0 then
			self.map_result = result
			self.stage = 'reduce'
		end
	end

	if self.stage == 'reduce' then
		self:run_reduce()
		self.ok = true
	end
end

return M
