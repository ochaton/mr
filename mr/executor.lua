local M = {}

function M.setup(obj)
	local clock = require 'clock'
	local fiber = require 'fiber'
	local log = require 'log'

	assert(type(obj) == 'table', "obj must be a table")

	log.info("Setting up new job")

	local function decompile(what)
		if type(what) ~= 'table' then
			return nil
		end
		if ({string=true,number=true,boolean=true,['nil']=true,cdata=true})[what.t] then
			return what.v
		elseif what.t == 'function' then
			local ok, func = pcall(loadstring, what.v)
			if not ok then
				error(func)
			end
			local fglobal = setmetatable({}, {__index=_G})
			setfenv(func, fglobal)
			return func
		elseif what.t == 'table' then
			local r = {}
			for k, v in pairs(what.v) do
				r[k] = decompile(v)
			end
			return r
		end
	end

	local self = {}
	for k, f in pairs(obj) do
		self[k] = decompile(f)
	end
	assert(self.job_id, "job_id is required")
	assert(self.job_name, "job_name is required")

	self.status = { run_at = clock.time(), processed = 0, job_id = self.job_id }
	self.result = {}
	self.deadline = tonumber(self.deadline) or (fiber.time()+3600*60)

	self.sleep_every = math.min(700, tonumber(self.sleep_every) or 100)
	self.sleep_seconds = tonumber(self.sleep_seconds) or 0.001

	self.session_id = box.session.id()

	log.info("Received job=%s:%s deadline:%s sleep: %s/%s session:%s",
		self.job_id, self.job_name, self.deadline, self.sleep_every, self.sleep_seconds, self.session_id)

	self.fiber = fiber.create(function()
		fiber.name('mr/'..self.job_name..'/'..self.session_id..'/'..self.job_id, {truncate = true})

		self.status.fiber_id = fiber.id()
		log.info("Starting fiber: %s", self.status.fiber_id)

		local ok, err = pcall(function()
			local space = box.space[self.space]
			if not space then
				error(("Space %s not found"):format(self.space))
			end

			local progress = space:len() / 1000
			local next_log_at = progress
			local status = self.status

			space:pairs()
				:enumerate()
				:map(function(no, tuple)
					if no % self.sleep_every == 0 then
						fiber.sleep(self.sleep_seconds)
					end
					status.processed = no
					status.length = space:len()
					if next_log_at < no then
						log.info("[%.1f%%] processed in %.2fs",
							no / status.length*100, fiber.time()-status.run_at)
						next_log_at = next_log_at + progress
					end
					return tuple
				end)
				:take_while(function()
					return box.info.ro
						and box.session.exists(self.session_id)
						and fiber.time() < self.deadline
						and not self.stopped
				end)
				:map(function(tuple)
					local key, value = self:mapper(tuple)
					self.result[key] = value
					return 1
				end)
				:length()

			self.fiber = nil

			if not box.info.ro then
				log.warn("Node is not ro anymore")
				status.ok = false
				status.error = 'not master anymore'
				return
			elseif not box.session.exists(self.session_id) then
				log.warn("Session with controller was closed. terminating")
				status.ok = false
				status.error = 'session was closed'
				return
			elseif self.deadline and self.deadline < fiber.time() then
				log.warn("Deadline reached")
				status.ok = false
				status.error = 'deadline is reached'
				return
			elseif self.stopped then
				log.warn("job was manually cancelled")
				status.ok = false
				status.error = 'job was cancelled'
				return
			end
		end)

		if not ok then
			log.error("Loop failed with error %s", err)
			self.status.ok = false
			self.status.error = err
			return
		end

		if self.status.ok == nil then
			-- Success
			self.status.ok = true
			self.status.result = self.result
		end
	end)

	box.session.storage[self.job_id] = self

	function self.get_status()
		return self.status
	end

	function self.cancel()
		log.info("Received job cancel")
		self.stopped = true
		return
	end

	function self.teardown()
		self.status = nil
		self.result = nil
		local ok, err
		if self.fiber then
			local fstatus
			ok, fstatus = pcall(self.fiber.status, self.fiber)
			if ok and fstatus == 'dead' then
				self.fiber = nil
			else
				ok, err = pcall(self.fiber.cancel, self.fiber)
				if not ok then
					log.error("cancel of %s fiber failed: %s", self.job_name, err)
				else
					log.info("fiber was cancelled successfully")
				end
				self.fiber = nil
			end
		end
		collectgarbage('collect')
		box.session.storage[self.job_id] = nil
		return "ok"
	end

	log.info("job=%s/%s started", self.job_name, self.job_id)

	return self.get_status()
end


return M