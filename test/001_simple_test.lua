local t = require 'luatest'
local g = t.group('simple')

local mr = require 'mr'

g.test_interface = function()
	mr {
		space = 'files',
		pair = {},
	}
	:map(function(self, file)
		local pair_id = file.pair_id
		if not self.pair[pair_id] then
			self.pair[pair_id] = { size = 0ULL, count = 0 }
		end
		local pair = self.pair[pair_id]
		pair.size = pair.size + file.size
		pair.count = pair.count + 1
		return pair_id, self.pair
	end)
	:reduce(function(self, pair_id, pair)
		if not self.pair[pair_id] then
			self.pair[pair_id] = { size = 0ULL, count = 0 }
		end
		local res = self.pair[pair_id]
		res.size = res.size + pair.size
		res.count = res.count + pair.count
		return pair_id, self.pair
	end)
end